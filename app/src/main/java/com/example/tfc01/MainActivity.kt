package com.example.tfc01

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity

class MainActivity : AppCompatActivity() {

    lateinit var input: TextView
    lateinit var history: TextView
    lateinit var numberButton: Button
    lateinit var opButton: Button
    lateinit var clearButton: Button
    var inputValue: String = ""
    var operationValue: String = ""
    var numbersList: ArrayList<String> = ArrayList()
    var operationsList: ArrayList<String> = ArrayList()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        input = findViewById(R.id.inputTextView)
        history = findViewById(R.id.historyTextView)
    }

    fun onClickNumber(v: View) {
        numberButton = v as Button
        inputValue += numberButton.getText().toString()
        input.setText(inputValue)
    }

    fun onClickOperation(v: View) {
        opButton = v as Button
        operationValue = opButton.getText().toString()

        if(inputValue.isNotEmpty()) {
            numbersList.add(inputValue)
            if(!operationValue.equals(OperationEnum.EQUALS.value)) {
                operationsList.add(operationValue)
            }
        }

        if(history.getText().isEmpty()){
            history.setText(String.format("%s%s", inputValue, operationValue))
            input.setText("0")
        } else if(inputValue.isEmpty()) {
            var simbol : String = history.getText().toString()
            history.setText(simbol.replaceRange(simbol.length - 1, simbol.length, operationValue))
        } else {
            numbersList.add(calculate())
            updateHistory()
            input.setText(numbersList.get(numbersList.size - 1))
        }
        inputValue = "";
    }

    fun updateHistory() {
        var operation: String = if(operationsList.size > 1)
            operationsList.get(operationsList.size - 2) else
                operationsList.get(operationsList.size - 1)

        history.setText(String.format("%s %s %s %s",
            numbersList.get(numbersList.size - 3),
            operation,
            numbersList.get(numbersList.size - 2),
            operationValue
        ))
    }

    fun calculate() : String {
        var num1: Double = numbersList.get(numbersList.size - 2).toDouble()
        var num2: Double = inputValue.toDouble()
        var operationSymbol : String = if (operationValue.equals(OperationEnum.EQUALS.value))
        operationsList.get(operationsList.size - 1) else operationsList.get(operationsList.size - 2)

        if(operationSymbol.equals(OperationEnum.SUM.value)){
            return (num1 + num2).toString();
        }
        if(operationSymbol.equals(OperationEnum.MINUS.value)){
            return (num1 - num2).toString();
        }
        if(operationSymbol.equals(OperationEnum.MULTIPLY.value)){
            return (num1 * num2).toString();
        }
        else {
            return (num1 / num2).toString();
        }
    }

    fun onClickClear(v:View){
        clearButton = v as Button
        if(clearButton.text.equals(Clear.C.value)){
            input.setText("0")
            inputValue=""
            history.setText("")
        }
        if(clearButton.text.equals(Clear.CE.value)){
            input.setText("0")
            inputValue=""
        }
        if(clearButton.text.equals(Clear.BACKSPACE.value)){
            var back = input.text
            input.setText(back.dropLast(1))
        }

    }

    enum class OperationEnum(val value: String) {
        SUM("+"),
        MINUS("-"),
        MULTIPLY("*"),
        DIVIDE("/"),
        EQUALS("=");
    }

    enum class Clear(val value: String) {
        C("C"),
        CE("CE"),
        BACKSPACE("<")
    }

}